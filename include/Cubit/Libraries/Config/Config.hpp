#pragma once

#include "Cubit/Libraries/Exceptions/Exceptions.hpp"

#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <filesystem>

namespace Cubit::Libraries::Config {
    class ConfigSectionBuilder;
    
    class ConfigSection {
        std::vector<std::vector<std::pair<std::string, size_t>>> locations;
        std::vector<std::string> names;
        
        std::vector<std::shared_ptr<ConfigSection>> sections;
        std::vector<std::string> values;

        static size_t stringBucket(std::string string);
        
        size_t findLocation(std::string name) const;
    
    public:
        explicit ConfigSection(ConfigSectionBuilder* builder);
        
        ~ConfigSection();
        
        std::vector<std::string> getKeys() const;
        
        bool hasKey(std::string key) const;
        
        std::string getValue(std::string key, bool doThrow = true, std::string defaultValue = "") const;
        
        std::string getString(std::string key, bool doThrow = true, std::string defaultValue = "") const;
        
        bool getBool(std::string key, bool doThrow = true, bool defaultValue = false) const;
        
        char getChar(std::string key, bool doThrow = true, char defaultValue = 0) const;
        
        short getShort(std::string key, bool doThrow = true, short defaultValue = 0) const;
        
        int getInt(std::string key, bool doThrow = true, int defaultValue = 0) const;
        
        long long getLongLong(std::string key, bool doThrow = true, long long defaultValue = 0) const;
        
        int8_t getInt8(std::string key, bool doThrow = true, int8_t defaultValue = 0) const;
        
        int16_t getInt16(std::string key, bool doThrow = true, int16_t defaultValue = 0) const;
        
        int32_t getInt32(std::string key, bool doThrow = true, int32_t defaultValue = 0) const;
        
        int64_t getInt64(std::string key, bool doThrow = true, int64_t defaultValue = 0) const;
        
        uint8_t getUint8(std::string key, bool doThrow = true, uint8_t defaultValue = 0) const;
        
        uint16_t getUint16(std::string key, bool doThrow = true, uint16_t defaultValue = 0) const;
        
        uint32_t getUint32(std::string key, bool doThrow = true, uint32_t defaultValue = 0) const;
        
        uint64_t getUint64(std::string key, bool doThrow = true, uint64_t defaultValue = 0) const;
        
        float getFloat(std::string key, bool doThrow = true, float defaultValue = 0) const;
        
        double getDouble(std::string key, bool doThrow = true, double defaultValue = 0) const;
        
        long double getLongDouble(std::string key, bool doThrow = true, long double defaultValue = 0) const;
        
//        template<typename T>
//        T getType(std::string key, bool doThrow = true) const;
//
//        template<typename T>
//        T getType(std::string key, T defaultValue, bool doThrow = true) const;

        /*
         * used internally, i know you cant use it
         */
        template<typename T>
        T getLexicalType(std::string key, bool doThrow = true, T defaultValue = 0) const;
        
        std::shared_ptr<ConfigSection>
        getSubsection(std::string key, bool doThrow = true,
                      std::shared_ptr<ConfigSection> defaultValue = nullptr) const;
    };
    
    std::unique_ptr<ConfigSection> readConfigFile(std::filesystem::path path);
    
    void writeConfigToFile(std::filesystem::path path, ConfigSection* config);
    
    class InvalidConfigSyntax : public Exceptions::InvalidArgument {
    public:
        InvalidConfigSyntax(const char* function, const char* file, int linenum,
                            const std::__cxx11::string& message);
    };
    
    class InvalidConfigName : public Exceptions::InvalidArgument {
    public:
        InvalidConfigName(const char* function, const char* file, int linenum, const std::__cxx11::string& message);
    };
    
    class InvalidConfigType : public Exceptions::InvalidArgument {
    public:
        InvalidConfigType(const char* function, const char* file, int linenum, const std::__cxx11::string& message);
    };
}