#include "Cubit/Libraries/Config/Config.hpp"

#include <functional>
#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/case_conv.hpp>

//#include <boost/property_tree/ptree.hpp>
//#include <boost/property_tree/ini_parser.hpp>
//#include <boost/property_tree/json_parser.hpp>


namespace Cubit::Libraries::Config {
    
    /**
    * must be one less than a power of two due to how the "hashing" is done (bitwise compare)
    * add one for actual bucket count
    * see Config.cpp#L230 (size_t ConfigSection::stringBucket(std::string)) for details
    */
    static constexpr size_t bucketCount = 15;
    
    class ConfigSectionBuilder {
        //TODO: duplicate name check
    public:
        std::vector<std::pair<std::string, std::shared_ptr<ConfigSectionBuilder>>> subsections;
        std::vector<std::pair<std::string, std::string>> values;
        
        void addSubSection(std::string name, std::shared_ptr<ConfigSectionBuilder> builder) {
            CUBIT_STACKTRACE
            std::pair<std::string, std::shared_ptr<ConfigSectionBuilder>> pair;
            pair.first = std::move(name);
            pair.second = std::move(builder);
            subsections.emplace_back(pair);
        }
        
        void addValue(std::string name, std::string value) {
            CUBIT_STACKTRACE
            std::pair<std::string, std::string> pair;
            pair.first = std::move(name);
            pair.second = std::move(value);
            values.emplace_back(pair);
        }
    };
    
    std::unique_ptr<ConfigSection> readConfigFile(std::filesystem::path path) {
        CUBIT_STACKTRACE
        
        std::vector<std::filesystem::path> previousConfigs;
        
        std::function<std::shared_ptr<ConfigSectionBuilder>(std::filesystem::path)> loadConfig =
                [&](std::filesystem::path path) {
                    for (const auto& previousConfig : previousConfigs) {
                        if (boost::filesystem::canonical(boost::filesystem::path(previousConfig)) ==
                            boost::filesystem::canonical(boost::filesystem::path(path))) {
                            throw Exceptions::InvalidRecursion(CUBIT_EXCEPTION_INFO,
                                                               "Config file contains looping include: " +
                                                               path.string()
                            );
                        }
                    }
                    previousConfigs.emplace_back(path);
                    
                    std::shared_ptr<ConfigSectionBuilder> baseSectionBuilder(new ConfigSectionBuilder());
                    
                    std::ifstream inputStream(path.string().c_str());
                    if (!inputStream.is_open()) {
                        throw Exceptions::FileNotFound(
                                CUBIT_EXCEPTION_INFO, "Could not find config file: " + path.string());
                    }
                    std::stringstream stringstream;
                    stringstream << inputStream.rdbuf();
                    std::string fileContents = stringstream.str();
                    
                    std::shared_ptr<ConfigSectionBuilder> currentBuilder = baseSectionBuilder;
                    
                    for (const char* currentCharacter = fileContents.c_str(); *currentCharacter != 0;) {
                        switch (*currentCharacter) {
                            case '[': {
                                // head new section
                                std::string line;
                                for (; *currentCharacter != 0 && *currentCharacter != '\n'; currentCharacter++) {
                                    line += *currentCharacter;
                                }
                                currentCharacter++;
                                
                                boost::trim(line);
                                
                                if (line.find(']') == -1) {
                                    throw InvalidConfigSyntax(
                                            CUBIT_EXCEPTION_INFO,
                                            "Section \"" + line + "\" in \"" + path.string() + "\" not ended");
                                }
                                
                                size_t firstOpenBracket = line.find('[');
                                size_t lastOpenBracket = line.rfind('[');
                                size_t firstCloseBracket = line.find(']');
                                size_t lastCloseBracket = line.rfind(']');
                                if (
                                        firstOpenBracket != lastOpenBracket
                                        || firstCloseBracket != lastCloseBracket
                                        || firstOpenBracket >= lastCloseBracket
                                        || firstOpenBracket == -1
                                        || firstCloseBracket == -1
                                        ) {
                                    throw InvalidConfigSyntax(
                                            CUBIT_EXCEPTION_INFO, "Invalid config section name");
                                }
                                
                                std::string name = line.substr(firstOpenBracket + 1, firstCloseBracket - 1);
                                
                                std::shared_ptr<ConfigSectionBuilder> newBuilder(new ConfigSectionBuilder());
                                baseSectionBuilder->addSubSection(name, newBuilder);
                                currentBuilder = newBuilder;
                                break;
                            }
                            case ';': {
                                while (*currentCharacter != 0 && *currentCharacter != '\n') {
                                    currentCharacter++;
                                }
                                if (*currentCharacter == '\n') {
                                    currentCharacter++;
                                }
                                break;
                            }
                            default: {
                                std::string name = "";
                                for (; *currentCharacter != 0 && *currentCharacter != '=' &&
                                       *currentCharacter != '\n'; currentCharacter++) {
                                    name += *currentCharacter;
                                }
                                bool foundEqual = (*currentCharacter == '=');
                                currentCharacter++;
                                
                                std::string value;
                                if (currentCharacter[-1] != '\n') {
                                    for (; *currentCharacter != 0 && *currentCharacter != '\n'; currentCharacter++) {
                                        value += *currentCharacter;
                                    }
                                }
                                boost::trim(name);
                                boost::trim(value);
                                
                                if (name.empty()) {
                                    continue;
                                }
                                
                                if (!foundEqual) {
                                    // subfile
                                    auto subfile = loadConfig({path.parent_path().string() + "/" + name});
                                    currentBuilder->addSubSection(name, subfile);
                                } else {
                                    currentBuilder->addValue(name, value);
                                }
                                
                                break;
                            }
                            
                        }
                    }
                    previousConfigs.pop_back();
                    return baseSectionBuilder;
                };
        
        return std::make_unique<ConfigSection>(loadConfig(std::move(path)).get());
    }

//    boost::property_tree::ptree readPtree(std::filesystem::path path) {
//        namespace pt = boost::property_tree;
//        pt::ptree tree;
//        pt::ini_parser::read_ini("ini", tree);
//        std::cout << tree.get<std::string>("key") << std::endl;
//        for (const auto& item : tree) {
//
//        }
//
//        return boost::property_tree::ptree();
//    }

//    void writeConfigToFile(std::filesystem::path path, ConfigSection* config) {
//        CUBIT_STACKTRACE
//    }
//
    
    ConfigSection::ConfigSection(ConfigSectionBuilder* builder) {
        CUBIT_STACKTRACE
        
        locations.resize(bucketCount + 1);
        names.reserve(builder->values.size() + builder->subsections.size());
        
        auto placeLocationPair = [&](std::pair<std::string, size_t> pair) {
            names.emplace_back(pair.first);
            auto& vector = locations[stringBucket(pair.first)];
            vector.emplace_back(pair);
        };
        
        
        CUBIT_RESTACKTRACE
        
        sections.reserve(builder->subsections.size());
        for (const auto& subsectionBuilder : builder->subsections) {
            auto subsection = new ConfigSection(subsectionBuilder.second.get());
            sections.emplace_back(subsection);
            std::pair<std::string, size_t> locationPair;
            locationPair.first = subsectionBuilder.first;
            locationPair.second = sections.size() - 1;
            placeLocationPair(locationPair);
        }
        
        CUBIT_RESTACKTRACE
        
        values.reserve(builder->values.size());
        for (const auto& value : builder->values) {
            values.emplace_back(value.second);
            std::pair<std::string, size_t> locationPair;
            locationPair.first = value.first;
            locationPair.second = values.size() - 1;
            placeLocationPair(locationPair);
        }
        
        CUBIT_RESTACKTRACE
    }
    
    ConfigSection::~ConfigSection() {
        CUBIT_STACKTRACE
    }
    
    size_t ConfigSection::stringBucket(std::string string) {
        CUBIT_STACKTRACE
        boost::algorithm::to_lower(string);
        if (string.empty()) {
            return 0;
        }
        char firstChar = string[0];
        return firstChar & bucketCount;
    }
    
    size_t ConfigSection::findLocation(std::string name) const {
        CUBIT_STACKTRACE
        boost::algorithm::to_lower(name);
        auto& bucket = locations[stringBucket(name)];
        for (const auto& pair : bucket) {
            if (pair.first == name) {
                return pair.second;
            }
        }
        throw InvalidConfigName(CUBIT_EXCEPTION_INFO, "Could not find config name \"" + name + "\"");
    }
    
    std::vector<std::string> ConfigSection::getKeys() const {
        return names;
    }
    
    bool ConfigSection::hasKey(std::string key) const {
        CUBIT_STACKTRACE
        boost::algorithm::to_lower(key);
        auto& bucket = locations[stringBucket(key)];
        for (const auto& pair : bucket) {
            if (pair.first == key) {
                return true;
            }
        }
        return false;
    }
    
    std::string ConfigSection::getValue(std::string key, bool doThrow, std::string defaultValue) const {
        CUBIT_STACKTRACE
        auto location = findLocation(std::move(key));
        return values[location];
    }
    
    std::string ConfigSection::getString(std::string key, bool doThrow, std::string defaultValue) const {
        CUBIT_STACKTRACE
        if (!(doThrow || hasKey(key))) {
            return defaultValue;
        }
        return getValue(key, doThrow);
    }
    
    bool ConfigSection::getBool(std::string key, bool doThrow, bool defaultValue) const {
        CUBIT_STACKTRACE
        if (!(doThrow || hasKey(key))) {
            return defaultValue;
        }
        // lexical cast does not work with "true" or "false" inputs, so i need to explicitly check for that
        auto val = getValue(std::move(key), doThrow);
        boost::trim(val);
        boost::to_lower(val);
        if (val == "true") {
            return true;
        }
        if (val == "false") {
            return false;
        }
        // allows input of numbers not 0 and 1 for boolean, 42 for example
        try {
            return static_cast<bool>(boost::lexical_cast<int>(val));
        } catch (boost::bad_lexical_cast& e) {
            throw InvalidConfigType(
                    CUBIT_EXCEPTION_INFO,
                    "cannot convert" + key + " : " + val + " into " + e.target_type().name() + " type"
            );
        }
    }
    
    char ConfigSection::getChar(std::string key, bool doThrow, char defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<char>(std::move(key), doThrow);
    }
    
    short ConfigSection::getShort(std::string key, bool doThrow, short defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<short>(std::move(key), doThrow);
    }
    
    int ConfigSection::getInt(std::string key, bool doThrow, int defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<int>(std::move(key), doThrow);
    }
    
    long long ConfigSection::getLongLong(std::string key, bool doThrow, long long defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<long long>(std::move(key), doThrow);
    }
    
    int8_t ConfigSection::getInt8(std::string key, bool doThrow, int8_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<int8_t>(std::move(key), doThrow);
    }
    
    int16_t ConfigSection::getInt16(std::string key, bool doThrow, int16_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<int16_t>(std::move(key), doThrow);
    }
    
    int32_t ConfigSection::getInt32(std::string key, bool doThrow, int32_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<int32_t>(std::move(key), doThrow);
    }
    
    int64_t ConfigSection::getInt64(std::string key, bool doThrow, int64_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<int64_t>(std::move(key), doThrow);
    }
    
    uint8_t ConfigSection::getUint8(std::string key, bool doThrow, uint8_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<uint8_t>(std::move(key), doThrow);
    }
    
    uint16_t ConfigSection::getUint16(std::string key, bool doThrow, uint16_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<uint16_t>(std::move(key), doThrow);
    }
    
    uint32_t ConfigSection::getUint32(std::string key, bool doThrow, uint32_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<uint32_t>(std::move(key), doThrow);
    }
    
    uint64_t ConfigSection::getUint64(std::string key, bool doThrow, uint64_t defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<uint64_t>(std::move(key), doThrow);
    }
    
    float ConfigSection::getFloat(std::string key, bool doThrow, float defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<float>(std::move(key), doThrow);
    }
    
    double ConfigSection::getDouble(std::string key, bool doThrow, double defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<double>(std::move(key), doThrow);
    }
    
    long double ConfigSection::getLongDouble(std::string key, bool doThrow, long double defaultValue) const {
        CUBIT_STACKTRACE
        return getLexicalType<long double>(std::move(key), doThrow);
    }
    
//    template<typename T>
//    T ConfigSection::getType(std::string key, bool doThrow) const {
//        CUBIT_STACKTRACE
//        return T(getValue(std::move(key), doThrow));
//    }
//
//    template<typename T>
//    T ConfigSection::getType(std::string key, T defaultValue, bool doThrow) const {
//        CUBIT_STACKTRACE
//        if (!(doThrow || hasKey(key))) {
//            return defaultValue;
//        }
//        return T(getValue(std::move(key), doThrow));
//    }
    
    template<typename T>
    T ConfigSection::getLexicalType(std::string key, bool doThrow, T defaultValue) const {
        CUBIT_STACKTRACE
        if (!(doThrow || hasKey(key))) {
            return defaultValue;
        }
        try {
            return boost::lexical_cast<T>(getValue(key));
        } catch (boost::bad_lexical_cast& e) {
            if(!doThrow){
                return defaultValue;
            }
            throw InvalidConfigType(
                    CUBIT_EXCEPTION_INFO,
                    "cannot convert " + key + " : " + getValue(key) + " into " + e.target_type().name() +
                    " type"
            );
        }
    }
    
    std::shared_ptr<ConfigSection>
    ConfigSection::getSubsection(std::string key, bool doThrow, std::shared_ptr<ConfigSection> defaultValue) const {
        return sections[findLocation(std::move(key))];
    }
    
    InvalidConfigSyntax::InvalidConfigSyntax(const char* function, const char* file, int linenum,
                                             const std::__cxx11::string& message)
            : InvalidArgument(function, file, linenum, message, "InvalidConfigSyntax") {
    }
    
    InvalidConfigName::InvalidConfigName(const char* function, const char* file, int linenum,
                                         const std::__cxx11::string& message)
            : InvalidArgument(function, file, linenum, message, "InvalidConfigName") {
        
    }
    
    InvalidConfigType::InvalidConfigType(const char* function, const char* file, int linenum,
                                         const std::__cxx11::string& message)
            : InvalidArgument(function, file, linenum, message, "InvalidConfigName") {
    }
}